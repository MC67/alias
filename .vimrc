" Basic 
set nocompatible              " be iMproved, required
set number
set relativenumber
syntax enable
filetype plugin on

"Finding files
set path+=**
set wildmenu

"File browsing

let g:netrw_banner=0
let g:netrw_browse_split=4
let g:netrw_altv=1
let g:netrw_listyle=3

"Autocomplete
"command! MakeTag !ctags -R

" Highlight
set cursorline

" Mode Présentation 
function! PresentationShortcut()
    set nornu nonu nocursorline
    map n : bnext<CR>
    map p : bprev<CR>
    echom "Mode présentation"
endfunction

nnoremap <F8> :call GetDate('')<CR>
function! GetDate(format)
  let format = empty(a:format) ? '+%A %Y-%m-%d %H:%M UTC' : a:format
  let cmd = '/bin/date -u ' . shellescape(format)
  let result = substitute(system(cmd), '[\]\|[[:cntrl:]]', '', 'g')
  " Append space + result to current line without moving cursor.
  call setline(line('.'), getline('.') . ' ' . result)
endfunction
